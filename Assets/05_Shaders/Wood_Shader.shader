// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Wood_Shader"
{
	Properties
	{
		_BaseColor("Base Color", Color) = (0,0,0,0)
		_Diffuse("Diffuse", 2D) = "white" {}
		_Normal("Normal", 2D) = "bump" {}
		_Roughness("Roughness", 2D) = "white" {}
		_UVRotation("UV Rotation", Float) = 0
		_UVTiling("UV Tiling", Float) = 1
		_RoughnessAmount("Roughness Amount", Range( 0 , 2)) = 0
		_NormalIntensity("Normal Intensity", Range( 0 , 3)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float _NormalIntensity;
		uniform sampler2D _Normal;
		uniform float _UVTiling;
		uniform float _UVRotation;
		uniform sampler2D _Diffuse;
		uniform float4 _BaseColor;
		uniform sampler2D _Roughness;
		uniform float _RoughnessAmount;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float cos6 = cos( _UVRotation );
			float sin6 = sin( _UVRotation );
			float2 rotator6 = mul( ( i.uv_texcoord * _UVTiling ) - float2( 0,0 ) , float2x2( cos6 , -sin6 , sin6 , cos6 )) + float2( 0,0 );
			float3 tex2DNode11 = UnpackNormal( tex2D( _Normal, rotator6 ) );
			float3 appendResult22 = (float3(( _NormalIntensity * tex2DNode11.r ) , ( _NormalIntensity * tex2DNode11.g ) , tex2DNode11.b));
			o.Normal = appendResult22;
			o.Albedo = ( tex2D( _Diffuse, rotator6 ) * _BaseColor ).rgb;
			o.Smoothness = ( 1.0 - ( tex2D( _Roughness, rotator6 ) * _RoughnessAmount ) ).r;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15700
2537;415;1296;619;1332.548;359.4295;1.3;True;True
Node;AmplifyShaderEditor.RangedFloatNode;7;-1612.627,37.25044;Float;False;Property;_UVTiling;UV Tiling;5;0;Create;True;0;0;False;0;1;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;5;-1678.757,-159.328;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;3;-1368.475,34.11176;Float;False;Property;_UVRotation;UV Rotation;4;0;Create;True;0;0;False;0;0;1.6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-1365.015,-87.43951;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RotatorNode;6;-1156.389,-88.24953;Float;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;16;-771.3188,440.7385;Float;True;Property;_Roughness;Roughness;3;0;Create;True;0;0;False;0;2f5b288ebd6da884dbebfb0941c12629;4f14d21f0331ab745945bb750874fc43;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;18;-788.3527,691.9906;Float;False;Property;_RoughnessAmount;Roughness Amount;6;0;Create;True;0;0;False;0;0;1.97;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;11;-770.7816,229.7352;Float;True;Property;_Normal;Normal;2;0;Create;True;0;0;False;0;b58dde1cff8b842459ef87e9f2aa0b62;48e09741f58ba264baa5b71732458e08;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;19;-771.5029,144.7871;Float;False;Property;_NormalIntensity;Normal Intensity;7;0;Create;True;0;0;False;0;0;0.7;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-179.0067,-14.57016;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;-177.1955,76.91746;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;-389.397,529.8154;Float;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;2;-748.4024,-387.2231;Float;True;Property;_Diffuse;Diffuse;1;0;Create;True;0;0;False;0;a0e27118b8849af47888aa7ba1c115ea;07c839d2879fbe6498ee54ec899cf72c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;27;-681.8094,-124.5602;Float;False;Property;_BaseColor;Base Color;0;0;Create;True;0;0;False;0;0,0,0,0;0.9622642,0.7321342,0.494749,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;22;104.6754,10.44003;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-89.75235,-173.3674;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;15;-131.1161,449.2685;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;390.1156,-62.08659;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Wood_Shader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;10;0;5;0
WireConnection;10;1;7;0
WireConnection;6;0;10;0
WireConnection;6;2;3;0
WireConnection;16;1;6;0
WireConnection;11;1;6;0
WireConnection;20;0;19;0
WireConnection;20;1;11;1
WireConnection;23;0;19;0
WireConnection;23;1;11;2
WireConnection;17;0;16;0
WireConnection;17;1;18;0
WireConnection;2;1;6;0
WireConnection;22;0;20;0
WireConnection;22;1;23;0
WireConnection;22;2;11;3
WireConnection;24;0;2;0
WireConnection;24;1;27;0
WireConnection;15;0;17;0
WireConnection;0;0;24;0
WireConnection;0;1;22;0
WireConnection;0;4;15;0
ASEEND*/
//CHKSM=DA620BE14C114827D40FE42A361B5816ED9D2F00
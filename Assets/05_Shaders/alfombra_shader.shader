// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "alfombra_shader"
{
	Properties
	{
		_Tiling("Tiling", Float) = 1
		_PatronTile("Patron Tile", Vector) = (1,1,0,0)
		_alfombrabasecolor("alfombra basecolor", 2D) = "white" {}
		_AlfombraPatron("Alfombra Patron", 2D) = "white" {}
		_Normal("Normal", 2D) = "bump" {}
		_NormalIntensity("Normal Intensity", Float) = 1
		_Roughness("Roughness", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float _NormalIntensity;
		uniform sampler2D _Normal;
		uniform float _Tiling;
		uniform sampler2D _AlfombraPatron;
		uniform float2 _PatronTile;
		uniform sampler2D _alfombrabasecolor;
		uniform sampler2D _Roughness;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 temp_output_14_0 = ( i.uv_texcoord * _Tiling );
			float3 tex2DNode8 = UnpackNormal( tex2D( _Normal, temp_output_14_0 ) );
			float2 appendResult9 = (float2(( _NormalIntensity * tex2DNode8.r ) , ( _NormalIntensity * tex2DNode8.g )));
			float3 appendResult12 = (float3(appendResult9 , tex2DNode8.b));
			o.Normal = appendResult12;
			o.Albedo = ( tex2D( _AlfombraPatron, ( i.uv_texcoord * _PatronTile ) ) * tex2D( _alfombrabasecolor, temp_output_14_0 ) ).rgb;
			o.Smoothness = ( 1.0 - tex2D( _Roughness, temp_output_14_0 ) ).r;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15700
1988;259;1296;625;1621.811;-154.1109;1;True;True
Node;AmplifyShaderEditor.RangedFloatNode;15;-1631,334.3295;Float;False;Property;_Tiling;Tiling;0;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;13;-1685.886,106.0686;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;14;-1394.888,198.0686;Float;True;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;8;-1092.319,220.895;Float;True;Property;_Normal;Normal;4;0;Create;True;0;0;False;0;6d913ecc09b898d42a7a3ab49ec56785;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;5;-1002.322,134.8593;Float;False;Property;_NormalIntensity;Normal Intensity;5;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;3;-1587.148,-300.1431;Float;False;Property;_PatronTile;Patron Tile;1;0;Create;True;0;0;False;0;1,1;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;1;-1647.147,-474.1432;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;2;-1356.149,-382.1432;Float;True;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;-704.7172,235.5934;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-715.7172,134.5935;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;18;-1091.041,-88.96875;Float;True;Property;_alfombrabasecolor;alfombra basecolor;2;0;Create;True;0;0;False;0;9fe5f1c3add85ab44b76f4b72d71b72a;9fe5f1c3add85ab44b76f4b72d71b72a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;9;-549.0345,202.2441;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;4;-1096.077,-357.1418;Float;True;Property;_AlfombraPatron;Alfombra Patron;3;0;Create;True;0;0;False;0;6b233bc25c3bf2e4da1b6ab50c7b40af;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;16;-1107.902,456.0333;Float;True;Property;_Roughness;Roughness;6;0;Create;True;0;0;False;0;5c1c1faaf04fe22479c7a36b19ef4521;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;17;-397.0279,462.8311;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DynamicAppendNode;12;-405.53,236.3425;Float;False;FLOAT3;4;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;19;-753.754,-204.6434;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;alfombra_shader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;14;0;13;0
WireConnection;14;1;15;0
WireConnection;8;1;14;0
WireConnection;2;0;1;0
WireConnection;2;1;3;0
WireConnection;11;0;5;0
WireConnection;11;1;8;2
WireConnection;10;0;5;0
WireConnection;10;1;8;1
WireConnection;18;1;14;0
WireConnection;9;0;10;0
WireConnection;9;1;11;0
WireConnection;4;1;2;0
WireConnection;16;1;14;0
WireConnection;17;0;16;0
WireConnection;12;0;9;0
WireConnection;12;2;8;3
WireConnection;19;0;4;0
WireConnection;19;1;18;0
WireConnection;0;0;19;0
WireConnection;0;1;12;0
WireConnection;0;4;17;0
ASEEND*/
//CHKSM=1B736A1B73896C92CCC6945655E1C247B4CA1FFE
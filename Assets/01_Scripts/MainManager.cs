﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainManager : MonoBehaviour
{
    [SerializeField]
    private Animator selection;

    [SerializeField]
    private Image bar;

    [SerializeField]
    private Transform minutesText, locationText;

    [SerializeField]
    private Animator bg;

    public static bool bInit = false;

	// Use this for initialization
	void Start ()
    {
        if(bInit)
        {
            bg.Play("opacity");
            StartCoroutine(opacity());
        }
        else
        {
            bg.gameObject.SetActive(false);
            selection.SetBool("active", true);
        }
	}
	
	// Update is called once per frame
	void Update ()
    {

	}

    private IEnumerator opacity()
    {
        yield return new WaitForSeconds(0.5f);
        selection.SetBool("active", true);
    }

    private IEnumerator updateBar(int index)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(index);
        asyncLoad.allowSceneActivation = false;
        yield return new WaitForSeconds(1.1f);

        float time = 0.0f;
        float max = 10f;

        while(!asyncLoad.allowSceneActivation)
        {
            time += Time.deltaTime;
            float fill = time / max;
            bar.fillAmount = fill;

            float minFloat = Denormalize(fill, 200, 550);
            float locaFloat = Denormalize(fill, 550, 200);
            minutesText.GetComponent<RectTransform>().anchoredPosition = new Vector2(minFloat, minutesText.GetComponent<RectTransform>().anchoredPosition.y);
            locationText.GetComponent<RectTransform>().anchoredPosition = new Vector2(locaFloat, locationText.GetComponent<RectTransform>().anchoredPosition.y);

            if(asyncLoad.progress >= 0.9f && time >= max)
            {
                asyncLoad.allowSceneActivation = true;
            }

            yield return null;
        }
    }

    public void OpenLevel(int index)
    {
        switch(index)
        {
            case 1:
                selection.Play("first");
                break;
            case 2:
                selection.Play("second");
                break;
            case 3:
                selection.Play("third");
                break;
            case 4:
                selection.Play("four");
                break;
        }

        StartCoroutine(updateBar(1));

        bInit = true;
    }

    private float Denormalize(float normalize, float max, float min)
    {
        return normalize * (max - min) + min;
    }

    private float Normalize(float value, float max, float min)
    {
        return (value - min) / (max - min);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    private Animator bg, info;

    public static bool bInit = false;

    [SerializeField]
    private Texture2D spriteEnable, spriteDisable;

    [SerializeField]
    private RawImage infoIcon;

	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Return()
    {
        bg.Play("bgReturn");
        StartCoroutine(goBack());
    }

    private IEnumerator goBack()
    {
        yield return new WaitForSeconds(0.6f);
        SceneManager.LoadScene(0);
    }

    public void OpenInfo()
    {
        if (!info.GetBool("active"))
            info.SetBool("active", true);
        else
            info.SetBool("active", false);

        StartCoroutine(infoCoroutine(info.GetBool("active")));
    }

    private IEnumerator infoCoroutine(bool active)
    {
        Transform container = info.transform.Find("container");

        if(active)
        {
            infoIcon.texture = spriteDisable;
            int length = container.childCount;
            for(int i = 0; i < length; i++)
            {
                yield return disableplayChild(container.GetChild(i).GetComponent<Animator>());
            }
        }
        else
        {
            infoIcon.texture = spriteEnable;
            yield return new WaitForSeconds(0.4f);
            int length = container.childCount;
            for (int i = 0; i < length; i++)
            {
                yield return enableplayChild(container.GetChild(i).GetComponent<Animator>());
            }
        }

        yield return null;
    }

    private IEnumerator enableplayChild(Animator child)
    {
        child.Play("elementEnable");
        yield return new WaitForSeconds(0.2f);
    }

    private IEnumerator disableplayChild(Animator child)
    {
        child.Play("elementDisable");
        yield return new WaitForSeconds(0.2f);
    }
}
